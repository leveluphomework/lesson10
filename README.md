# lesson10

Deploy and configure nginx using jenkins

Выполнено: Сборка докер образа nginx alpine с изменением стартовой страницы.

Stage console output:
```
    Started by user admin
    Obtained Jenkinsfile from git https://gitlab.com/leveluphomework/lesson10.git
    [Pipeline] Start of Pipeline
    [Pipeline] node
    Running on LvlUp in /home/danil/Projects/DevOps/jenkinsDir/workspace/LvlUp/TestPipline
    [Pipeline] {
    [Pipeline] stage
    [Pipeline] { (Declarative: Checkout SCM)
    [Pipeline] checkout
    Selected Git installation does not exist. Using Default
    The recommended git tool is: NONE
    using credential JenkinsLvlUp
    Fetching changes from the remote Git repository
    > git rev-parse --resolve-git-dir /home/danil/Projects/DevOps/jenkinsDir/workspace/LvlUp/TestPipline/.git # timeout=10
    > git config remote.origin.url https://gitlab.com/leveluphomework/lesson10.git # timeout=10
    Fetching upstream changes from https://gitlab.com/leveluphomework/lesson10.git
    > git --version # timeout=10
    > git --version # 'git version 2.25.1'
    using GIT_ASKPASS to set credentials 
    > git fetch --tags --force --progress -- https://gitlab.com/leveluphomework/lesson10.git +refs/heads/*:refs/remotes/origin/* # timeout=10
    Checking out Revision 9f593a686a0821407bf0bb51a7a4bef637a2f172 (refs/remotes/origin/main)
    Commit message: "change image name"
    > git rev-parse refs/remotes/origin/main^{commit} # timeout=10
    > git config core.sparsecheckout # timeout=10
    > git checkout -f 9f593a686a0821407bf0bb51a7a4bef637a2f172 # timeout=10
    > git rev-list --no-walk b9e89f329fbb98d22c333732a37f33d53d593195 # timeout=10
    [Pipeline] }
    [Pipeline] // stage
    [Pipeline] withEnv
    [Pipeline] {
    [Pipeline] timestamps
    [Pipeline] {
    [Pipeline] stage
    [Pipeline] { (First)
    [Pipeline] sh
    00:38:45  + docker build -t nginx_test:latest .
    00:38:45  Sending build context to Docker daemon  96.26kB

    00:38:45  Step 1/4 : FROM nginx:stable-alpine
    00:38:45   ---> 373f8d4d4c60
    00:38:45  Step 2/4 : COPY ./index.html /usr/share/nginx/html
    00:38:45   ---> 502b96d834db
    00:38:45  Step 3/4 : EXPOSE 80:80
    00:38:45   ---> Running in 24b3fcd387ea
    00:38:45  Removing intermediate container 24b3fcd387ea
    00:38:45   ---> 2fcb66d3ed79
    00:38:45  Step 4/4 : CMD ["nginx", "-g", "daemon off;"]
    00:38:45   ---> Running in dc27387acbd4
    00:38:45  Removing intermediate container dc27387acbd4
    00:38:45   ---> a2cd8233f075
    00:38:45  Successfully built a2cd8233f075
    00:38:45  Successfully tagged nginx_test:latest
    [Pipeline] }
    [Pipeline] // stage
    [Pipeline] }
    [Pipeline] // timestamps
    [Pipeline] }
    [Pipeline] // withEnv
    [Pipeline] }
    [Pipeline] // node
    [Pipeline] End of Pipeline
    Finished: SUCCESS
```
Скрин собранного образа: https://gitlab.com/leveluphomework/lesson10/-/blob/main/dockerImagesList.png
